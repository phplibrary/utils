#SINEVIA UTILS

Class with utility functions

# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/utils.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/utils": "dev-master"
    },
```

# How to Use? #